<?php
/**
 * Created by PhpStorm.
 * User: ron
 * Date: 2017/07/21
 * Time: 3:07 PM
 */

namespace RonsBundle\Forms;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                    'attr' => [
                        'placeholder' => 'Full Name'
                    ],
                    'constraints' => [
                        new NotBlank(["message" => "Please provide your name"
                        ]),
                    ]]
            )
            ->add('email', EmailType::class, [
                    'attr' => [
                        'placeholder' => 'Email'
                    ],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Please provide a valid email'
                        ]),
                        new Email([
                            'message' => 'Your email doesn\'t seems to be valid'
                        ]),
                    ]]
            )
            ->add('message', TextareaType::class, [
                'attr' => [
                    'placeholder' => 'Your message here'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please provide a message here'
                    ]),
                ]
            ]);
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'error_bubbling' => true
        ]);
    }

    public function getName()
    {
        return 'contact_form';
    }
}