<?php

namespace RonsBundle\Controller;

use RonsBundle\Entity\Contacted;
use RonsBundle\Forms\ContactForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package RonsBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @var string
     */
    private $adminEmail = 'ron@opentechsolutions.co.za';
    private $adminName = 'Ron Darby';

    /**
     * @Route("/", name="contactpage")
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(ContactForm::class, null, [
            'action' => $this->generateUrl('contactpage'),
            'method' => 'POST'
        ]);

        // Capture and mail if form submitted
        if ($request->isMethod('POST')) {

            $form->handleRequest($request);

            //validate the form submition
            if ($form->isValid() && $this->verifyRecapta($request->get('g-recaptcha-response'))) {
                // get the doctrine entityManager
                $entityManager = $this->getDoctrine()->getManager();

                // Capture the relevent details
                $formData = $form->getData();

                // Instantiate and construct the entity
                $contact = new Contacted();
                $contact->setName($formData['name']);
                $contact->setEmail($formData['email']);
                $contact->setMessage($formData['message']);

                // Manage the Contacted entity
                $entityManager->persist($contact);

                // Insert the contact information into the database
                $entityManager->flush();

                // Notify the system admin of the contact
                $this->sendMail($formData);
                // Notify the client of a successfull message
                $this->sendMail($formData, false);

                $this->addFlash(
                    'success',
                    'Your message was sent!'
                );

                return $this->redirectToRoute('contactpage');
            }

        }
        // return $this->render('@RonsBundle/social/contact/index.html.twig', ['form' => $form->createView()]);
        return $this->render('social/contact/index.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @Route("/api/contacted", name="contactapi")
     * @Method("POST")
     */
    public function createContactedAction(Request $request)
    {

        try {
            $name = $request->get('name');
            $email = $request->get('email');
            $message = $request->get('message');

            $contact = new Contacted();
            $contact->setName($name);
            $contact->setEmail($email);
            $contact->setMessage($message);

            $validator = $this->get('validator');
            $errors = $validator->validate($contact);

            if (count($errors) > 0) {
                $errorsString = (string)$errors;

                throw new \Exception($errorsString);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contact);
            $entityManager->flush();

            return new Response('Contacted Entry Created');
        } catch (\Exception $e) {
            return new Response($e->getMessage());
        }


    }

    /**
     * @param $mailData
     * @param bool $admin
     * @return mixed
     */
    private function sendMail($mailData, $admin = true)
    {


        $mailer = $this->container->get('swiftmailer.mailer');

        // Create a message
        if ($admin) {
            $to = [$this->adminEmail => $this->adminName];
            $from = [$mailData['email'] => $mailData['name']];
            $mail = 'We have been contacted by: ' . $mailData['name'] . ' - ' . $mailData['email'] . '\n\n';
            $mail .= $mailData['message'];
            $subject = 'Contact Request from RonsBundle';
        } else {
            $to = [$mailData['email'] => $mailData['name']];
            $from = [$this->adminEmail => $this->adminName];

            $mail = 'Thank-you for contacting us, we will contact you back as soon as possible';

            $subject = 'Your Contact Request';
        }

        $message = (new \Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody($mail);

        return $mailer->send($message);
    }

    /**
     * @param $recaptcha
     * @return mixed
     */
    private function verifyRecapta($recaptcha)
    {

        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $secretKey = $this->getParameter('g_recaptcha_secret');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'secret' => $secretKey,
            'response' => $recaptcha
        ]);
        $response = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($response);

        return $data->success;
    }

}
