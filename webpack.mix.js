const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .extract(['vue','vee-validate','vue-easy-toast','store','jquery','bootstrap-sass','underscore'])

   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/vendor.scss', 'public/css')
    .version();

